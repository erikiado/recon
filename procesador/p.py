import cv2
import numpy as np
import sys
import os

tamBatch = 300

pathArchivo = os.path.dirname(os.path.realpath(__file__))

###RANGOS########################
# [][0]inferior
# [][1]superior
rango = {}

rango['piel'] = [np.array([0,20,10]),
                 np.array([30,200,255])]

rango['rojo'] =  [np.array([170,20,10]),
                  np.array([179,230,255])]

rango['control'] = [np.array([0,0,0]),
                    np.array([0,0,0])]
#################################



###FUNCIONES

def nothing(x):
    pass


def encontrarMano(contours):
    areaMayor = 0
    contMayor = contours[0]
    for cnt in contours:
        area = cv2.contourArea(cnt)
        if(area > areaMayor):
            areaMayor = area
            contMayor = cnt

    maxX = maxY = 0
    minX = minY = 100000
    for i in contMayor:
        x = i[0][0]
        y = i[0][1]
        maxX = max(maxX, x)
        minX = min(minX, x)
        maxY = max(maxY, y)
        minY = min(minY, y)
    maxX += 50
    minX -= 50
    minY -= 50
    maxY = minY + ( maxX - minX )#+= 50 
    
    return minY,maxY,minX,maxX


def procesar(img):#default es rojo
    bilBlur = cv2.bilateralFilter(img,7,75,75)
    hsv = cv2.cvtColor(bilBlur, cv2.COLOR_BGR2HSV)
    mask =  cv2.inRange(hsv,rango['control'][0],rango['control'][1])# + cv2.inRange(hsv,rango['piel'][0],rango['piel'][1]) + cv2.inRange(hsv,rango['rojo'][0],rango['rojo'][1]) 
    #solo la mascara del control si se comenta la suma
    mk1 = cv2.inRange(hsv,rango['control'][0],rango['control'][1])# + cv2.inRange(hsv,rango['piel'][0],rango['piel'][1]) + cv2.inRange(hsv,rango['rojo'][0],rango['rojo'][1]) 
    return mask, mk1

def crearDirectorios(letra):
    pathOrg = "/dset/org/"+letra+"/"
    pathMask = "/dset/mask/"+letra+"/"
    path20 =  "/dset/20x20/"+letra+"/"
    path40 =  "/dset/40x40/"+letra+"/"
    pathMama =  "/dset/mama/"+letra+"/"
    pathMano =  "/dset/mano/"+letra+"/"
    wd = pathArchivo
    if not os.access(pathArchivo+"/dset",os.R_OK):
        os.mkdir(wd+"/dset")
        os.mkdir(wd+"/dset/org")
        os.mkdir(wd+"/dset/mask")
        os.mkdir(wd+"/dset/40x40")
        os.mkdir(wd+"/dset/20x20")
        os.mkdir(wd+"/dset/mama")
        os.mkdir(wd+"/dset/mano")
    os.mkdir(wd+pathMama)
    os.mkdir(wd+pathOrg)
    os.mkdir(wd+pathMano)
    os.mkdir(wd+pathMask)
    os.mkdir(wd+path20)
    os.mkdir(wd+path40)


def crearMinis(org,y,x):
    i20 = cv2.resize(org,(20,20))
    i40 = cv2.resize(org,(40,40))
    return i20,i40
    #cv2.imshow("MINIIII",img)
    # img = np.zeros((400,400), np.uint8)
    
    # if org is not None:
    #     cv2.imshow('MNI',org)
    # # # if max(x,y) == x:
    # #     y = int((400*y)/x)
    # #     x = 400
    # #     print(x,y)
    # #     org = cv2.resize(org,(y,x))
    # #     p = int((x-y)/2)
    # #     # print("p: "+str(p))
    # #     # print("img: "+str(len(img)))
    # #     # print("img0: "+str(len(img[0])))
    # #     # print("org: "+str(len(org)))
    # #     # print("org0: "+str(len(org[0])))
    # #     img[:,p:p+y] = org
    # # else:
    # #     x = int((400*x)/y)
    # #     y = 400
    # #     print(x,y)
    # #     org = cv2.resize(org,(x,y))
    # #     p = int((y-x)/2)  
    # #     # print("p: "+str(p))
    # #     # print("img: "+str(len(img)))
    # #     # print("img0: "+str(len(img[0])))
    # #     # print("org: "+str(len(org)))
    # #     # print("org0: "+str(len(org[0])))
    # #     img[p:p+x,:] = org
    # #m20 = cv2.resize(img,(40,40))
    #cv2.imshow('MINI',org)
    #cv2.imshow('40',m20)




if(len(sys.argv) > 2):
    ###SETUP

    camara = cv2.VideoCapture(0)

    font = cv2.FONT_HERSHEY_PLAIN

    _,original = camara.read()
    tomandoFrames = False
    contadorFrame = 0

    k3 = (3,3)
    k5 = (5,5)

    letra = str(sys.argv[1]).lower()
    pathMuestra = "muestras/"+letra+".png"
    pathOrg = "dset/org/"+letra+"/"
    pathMask = "dset/mask/"+letra+"/"
    path20 =  "dset/20x20/"+letra+"/"
    path40 =  "dset/40x40/"+letra+"/"
    pathMama =  "dset/mama/"+letra+"/"
    pathMano =  "dset/mano/"+letra+"/"

    batch = int(sys.argv[2])
    cont = batch * tamBatch

    finBatch = (batch*tamBatch)+tamBatch

    muestra = cv2.imread(pathMuestra,0)
    cv2.imshow(('Muestra de letra: '+letra),muestra)

    cv2.namedWindow('control')
    cv2.createTrackbar('HI', 'control', 0, 179, nothing)
    cv2.createTrackbar('HS', 'control', 25, 179, nothing)
    cv2.createTrackbar('SI', 'control', 40, 255, nothing)
    cv2.createTrackbar('SS', 'control', 240, 255, nothing)
    cv2.createTrackbar('VI', 'control', 50, 255, nothing)
    cv2.createTrackbar('VS', 'control', 190, 255, nothing) #############################

    guardar = False
    teclas = ""

    if not os.access(pathArchivo+"/"+pathMama,os.R_OK):
        crearDirectorios(letra)


    while(True):
        

        ###TOMAR FOTO   
        _,original = camara.read()
        presentacion = mask = original
        nombreImg = str(cont) + ".png"  

        hi = cv2.getTrackbarPos('HI','control')
        si = cv2.getTrackbarPos('SI','control')
        vi = cv2.getTrackbarPos('VI','control')
        hs = cv2.getTrackbarPos('HS','control')
        ss = cv2.getTrackbarPos('SS','control')
        vs = cv2.getTrackbarPos('VS','control')

        rango['control'][0] = np.array([hi,vi,si])
        rango['control'][1] = np.array([hs,vs,ss])
        ###FILTRO
        if contadorFrame % 5 == 0: #procesar cada tantos frames
            mask,mk1 = procesar(original)
            cv2.imshow('control',mask)
            _,contours,_ = cv2.findContours(mk1,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
            if len(contours) != 0 and tomandoFrames: #Si existen blobs encontrar el area del mayor
                minY,maxY,minX,maxX = encontrarMano(contours)
                if minY<maxY and minX < maxX:
                    yAurea = maxY#+int(1.61*(maxY-minY))
                    mano = original[minY:yAurea,minX:maxX]
                    mama = mask[minY:yAurea,minX:maxX]
                    mama = cv2.morphologyEx(mama, cv2.MORPH_CLOSE, k3)
                    if mama is not None:
                        cv2.imshow("j",mama)
                        X = maxX - minX
                        Y = yAurea - minY
                        img20,img40 = crearMinis(mama,Y,X)
                        guardar = True
            

        ###PRESENTACION
        cv2.putText(presentacion,("(Z) Tomar Frames: "+str(tomandoFrames)),(20,30),font,1,(0,0,0),1,cv2.LINE_AA)
        cv2.putText(presentacion,("Path: ./dset/$tipo/"+letra+"/"+nombreImg),(20,40),font,1,(0,0,0),1,cv2.LINE_AA)
        cv2.putText(presentacion,str(contadorFrame),(20,50),font,1,(0,0,0),1,cv2.LINE_AA)
        cv2.putText(presentacion,teclas,(20,60),font,1,(0,0,0),1,cv2.LINE_AA)


        cv2.imshow('Estado',presentacion)


        if guardar:
            cv2.imwrite(pathArchivo+'/'+pathMask+nombreImg,mask)
            cv2.imwrite(pathArchivo+'/'+pathMama+nombreImg,mama)
            cv2.imwrite(pathArchivo+'/'+pathMano+nombreImg,mano)
            cv2.imwrite(pathArchivo+'/'+pathOrg+nombreImg,original)
            cv2.imwrite(pathArchivo+'/'+path20+nombreImg,img20)
            cv2.imwrite(pathArchivo+'/'+path40+nombreImg,img40)
            cont += 1
            guardar = False

        contadorFrame += 1



        ###CONTROLES
        k = cv2.waitKey(30) & 0xff#Bitwise And: Abs #Escuchar Teclado    
        if k != 255:
            teclas += chr(k)
            if k == ord('z'):
                if tomandoFrames:
                    tomandoFrames = False
                else:
                    tomandoFrames = True
            elif k == 27:
                break
        if cont > finBatch:
            break


    camara.release()
    cv2.destroyAllWindows()

else:
    print("Chavo, te hacen falta parametros:\npython "+sys.argv[0]+" $letra $batch")